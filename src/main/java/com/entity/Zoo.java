package com.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Zoo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int zid;
    private String name;
    @OneToMany(mappedBy = "zoo",cascade = CascadeType.ALL)
    private List<Animal> animals;

    public Zoo() {
    }

    public Zoo( String name, List<Animal> animal) {
        this.zid = zid;
        this.name = name;
        this.animals = animals;
    }

    public int getZid() {
        return zid;
    }

    public void setZid(int zid) {
        this.zid = zid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Animal> getAnimal() {
        return animals;
    }

    public void setAnimal(List<Animal> animal) {
        this.animals = animals;
    }

    @Override
    public String toString() {
        return "Zoo{" +
                "zid=" + zid +
                ", name='" + name + '\'' +
                ", animals=" + animals +
                '}';
    }


    public  void addAnimal(Animal animal){
        if(animals==null){
            animals=new ArrayList<Animal>();
        }
        animals.add(animal);
    }
}
