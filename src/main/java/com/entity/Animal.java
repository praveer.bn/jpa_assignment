package com.entity;

import javax.persistence.*;

@Entity
public class Animal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int aid;
    private String aname;
    private String category;
    private  int age;
    @ManyToOne(cascade = CascadeType.ALL)
    private Zoo zoo;

    public Animal() {
    }

    public Animal( String aname, String category, int age) {
        this.aname = aname;
        this.category = category;
        this.age = age;
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "aid=" + aid +
                ", aname='" + aname + '\'' +
                ", category='" + category + '\'' +
                ", age=" + age +
                '}';
    }
}
