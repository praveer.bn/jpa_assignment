package com;

import com.entity.Animal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.stream.Collectors;

public class MainApplication {
    public static void main(String[] args) {
EntityManagerFactory entityManagerFactory= Persistence.createEntityManagerFactory("praveer");
        EntityManager entityManager=entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Object> criteriaQuery=criteriaBuilder.createQuery();
        Root<Animal> from=criteriaQuery.from(Animal.class);

        System.out.println("all records");
        CriteriaQuery<Object> select=criteriaQuery.select(from);
        TypedQuery<Object> typedQuery=entityManager.createQuery(select);
        List<Object> resultList=typedQuery.getResultList();

        resultList.stream().forEach(System.out::println);

            System.out.println("Select all records greater than 4 years");
            CriteriaQuery<Object> select1=criteriaQuery.select(from);
            select1.where(criteriaBuilder.greaterThan(from.get("age"),4));
            TypedQuery<Object> typedQuery1=entityManager.createQuery(select1);
            List<Object> resultlist1=typedQuery1.getResultList();

            resultlist1.stream().forEach(System.out::println);
            entityManagerFactory.close();
            entityManager.close();

            EntityManagerFactory entityManagerFactory1= Persistence.createEntityManagerFactory("praveer");
            EntityManager entityManager1=entityManagerFactory1.createEntityManager();
            CriteriaBuilder criteriaBuilder1=entityManager1.getCriteriaBuilder();
            CriteriaQuery<Object[]> criteriaQuery1=criteriaBuilder1.createQuery(Object[].class);
            Root<Animal> from1=criteriaQuery1.from(Animal.class);

            System.out.println("Select all records greater than 4 years and group by category");

//            select2.where(criteriaBuilder.greaterThan(from.get("age"),4));
            criteriaQuery1.multiselect(from1.get("category"),criteriaBuilder1.count(from1)).groupBy(from1.get("category"));
//            TypedQuery<Object[]> list=entityManager.createQuery(criteriaQuery1);

            System.out.print("category");
            System.out.println("\t Count");
            List<Object[]> list =entityManager1.createQuery(criteriaQuery1).getResultList();
            for(Object[] object : list){
                    System.out.println(object[0] + "     " + object[1]);

            }



            entityManagerFactory1.close();
            entityManager1.close();






    }
}
